# build environment
FROM node:20-alpine AS build


WORKDIR /app

RUN npm install -g pnpm@9.9.0

# install dependencies
COPY . .
RUN pnpm install 

# build app
RUN pnpm --filter doi-landing-page build


# production environment
FROM nginx:stable-alpine

COPY --from=build /app/apps/doi-landing-page/dist /usr/share/nginx/html/

COPY ./nginx.conf /etc/nginx/conf.d/default.conf


EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]