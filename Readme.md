# DOI landing page

## Introduction

This project implements the DOI landing page.
This landing page gives access to the metadata of the DOI and has a link to the data itself in the data portal.

## Structure

The project is composed of two subprojects:

- `apps/doi-landing-page`: The landing page itself. This is a react app which is in charge of resolving the requested route and use the relevant component to render the page.

- `packages/doi/`: The DOI package. Used by `apps/doi-landing-page` and published on npm registry for use by data portal. This package is in charge of fetching the metadata of the DOI and rendering it.

## Development

This project is using pnpm as package manager. You can install it with `npm install -g pnpm`.

To install the dependencies, run `pnpm install` at the root of the project.

To start developing, go to `apps/doi-landing-page` and run `pnpm dev`.

To publish a new version of the `doi` package:

- Go to `packages/doi`
- Update the version in `package.json`
- Run `pnpm package-publish`

## Build

To build the doi landing page app run `pnpm --filter doi-landing-page build`.

You can also build the docker image with the following command:

```sh
sudo docker build --progress=plain --no-cache -t doi .
```
