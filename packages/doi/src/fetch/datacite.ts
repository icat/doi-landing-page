import { Endpoint } from 'fetch/fetching';
import type { DataciteDOI } from 'model/datacite';

export const DATACITE_GET_DOI = Endpoint({
  server: 'DATACITE',
  path: 'dois/:doi',
  params: {} as {
    doi: string;
  },
  schema: {} as DataciteDOI,
});
