import { replaceEqualDeep, useSuspenseQuery } from '@tanstack/react-query';
import { CONFIG } from 'config/config';
import { useMemo } from 'react';

export const SERVERS = ['DATACITE', 'CITATION', 'USER_PORTAL'] as const;
export type Server = (typeof SERVERS)[number];

export type EndpointDefinitionParams<DATA, PARAMS> = {
  path: string;
  server: Server;
  schema?: DATA;
  params?: PARAMS;
};

export type EndpointDefinition<DATA, PARAMS> = Required<
  EndpointDefinitionParams<DATA, PARAMS>
>;

export function Endpoint<DATA, PARAMS>(
  args: EndpointDefinitionParams<DATA, PARAMS>,
): EndpointDefinition<DATA, PARAMS> {
  return {
    schema: {} as DATA,
    params: {} as PARAMS,
    ...args,
  };
}

function buildURL(endpoint: EndpointDefinition<unknown, unknown>, params: any) {
  const serverUrl = CONFIG.SERVERS[endpoint.server];
  const url = `${serverUrl}${endpoint.path}`;

  if (typeof params !== 'object' || !params) return url;

  const urlWithPathParams = Object.keys(params).reduce(
    (a, b) => a.replaceAll(`:${b}`, params[b]),
    url,
  );

  const queryParams = Object.keys(params).filter((key) => {
    return !url.includes(`:${key}`);
  });

  if (queryParams.length === 0) return urlWithPathParams;

  const queryParamsObject = new URLSearchParams();
  queryParams.forEach((key) => {
    queryParamsObject.append(key, params[key]);
  });

  return `${urlWithPathParams}?${queryParamsObject.toString()}`;
}

function fetchEndpoint<DATA, PARAMS>(args: {
  endpoint: EndpointDefinition<DATA, PARAMS>;
  params?: PARAMS;
  url: string;
  key?: any;
  skipFetch?: boolean;
  onFail?: (body: string) => void;
}) {
  if (args.skipFetch) return Promise.resolve(null);

  return fetch(args.url, {
    method: 'GET',
  })
    .catch((error) => {
      console.error(error);
      return null;
    })
    .then(async (response) => {
      const body = await response?.text();

      if (!response?.ok) {
        console.error(
          `Error fetching ${args.url}: ${response?.status} ${response?.statusText} ${body}`,
        );
        if (args.onFail)
          body ? args.onFail(body) : args.onFail('Error fetching data');
        return null;
      }
      if (!body) return null;
      try {
        return JSON.parse(body) as DATA;
      } catch (e) {
        return body as DATA;
      }
    });
}

export function getQueryObject<DATA, PARAMS>(args: {
  endpoint: EndpointDefinition<DATA, PARAMS>;
  params?: PARAMS;
  skipFetch?: boolean;
  onFail?: (body: string) => void;
}) {
  const url = buildURL(args.endpoint, args.params);

  const key = [args.params, url, args.skipFetch ? 'skip' : 'fetch'];

  return {
    queryKey: key,
    queryFn: () => fetchEndpoint({ ...args, url, key }),
  };
}

export function useLocalGetEndpoint<DATA, PARAMS, DEFAULT = undefined>(args: {
  endpoint: EndpointDefinition<DATA, PARAMS>;
  params?: PARAMS;
  default?: DEFAULT;
  skipFetch?: boolean;
  onFail?: (body: string) => void;
}): DATA | DEFAULT {
  const query = getQueryObject({
    endpoint: args.endpoint,
    params: args.params,

    skipFetch: args.skipFetch,
    onFail: args.onFail,
  });

  const d = useSuspenseQuery({
    notifyOnChangeProps: ['data'],
    structuralSharing: (oldData, newData) => {
      return replaceEqualDeep(oldData, newData);
    },
    refetchInterval: false,
    refetchIntervalInBackground: false,
    refetchOnMount: false,
    refetchOnReconnect: false,
    refetchOnWindowFocus: false,
    retry: false,
    ...query,
  });

  const res = useMemo(
    () =>
      d.data
        ? (JSON.parse(JSON.stringify(d.data)) as DATA) //make a copy so that the data is not mutated
        : (args.default as DEFAULT),
    [d.data], // eslint-disable-line react-hooks/exhaustive-deps
  );

  return res;
}
