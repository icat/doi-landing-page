import { Endpoint } from 'fetch/fetching';

export const USER_PORTAL_FILES = Endpoint({
  server: 'USER_PORTAL',
  path: 'reports/fileNames',
  params: {} as {
    categoryCode: string;
    categoryCounter: string;
  },
  schema: [] as string[],
});
