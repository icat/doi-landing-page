import { Endpoint } from 'fetch/fetching';

export const CITATION_FORMAT_REFERENCE = Endpoint({
  server: 'CITATION',
  path: 'format',
  params: {} as {
    style?: 'apa';
    lang?: 'en-US';
    doi: string;
  },
  schema: '' as string,
});
