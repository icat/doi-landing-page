export const RESTRICTED_ACCESS = 'Restricted access';
export const OPEN_ACCESS = 'Open access';

export const ES_RESOURCE_TYPE = 'Experiment Session';
export const DC_RESOURCE_TYPE = 'Datacollection';

export const CATEGORY_SUBJECT_SCHEME = 'Proposal Type Description';
export const INSTRUMENT_SUBJECT_SCHEME = 'Instrument';
export const PROPOSAL_SUBJECT_SCHEME = 'Proposal';

export const COLLECTED_DATE_TYPE = 'Collected';

export const ABSTRACT_DESCRIPTION_TYPE = 'Abstract';

export const ORCID_IDENTIFIER_SCHEME = 'ORCID';

export const COLLECTION_DATE_FORMAT = 'yyyy-MM-dd';

export const RELATED_IDENTIFIER_TYPE_RELATED_DOI = 'IsSupplementedBy';

export const RELATED_IDENTIFIER_TYPE_IS_CITED_BY = 'IsCitedBy';

export const RELATED_IDENTIFIER_TYPE_REFERENCES = 'References';
