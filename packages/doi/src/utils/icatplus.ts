import type { Instrument } from 'model/icatplus';

export function findInstrumentUrl(instruments: Instrument[], name: string) {
  const instrument = instruments.find((instrument) => instrument.name === name);
  return instrument ? instrument.url : undefined;
}
