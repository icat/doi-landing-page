/**
 * Extracts the proposalCode and the proposalNumber of a specific proposal name
 * @param {*} proposalName
 * @returns {proposalCode, proposalNumber}
 */
export function getProposalCodeAndNumber(proposalName: string) {
  const aroundHyphenRegExp = new RegExp(/^(.*)-(.*)/);
  try {
    // extract the proposal LetterCode (before the hyphen) and the digits (after the hyphen)
    const res = aroundHyphenRegExp.exec(proposalName);
    if (res?.length && res.length >= 2)
      return {
        proposalCode: res[1],
        proposalNumber: res[2],
      };
  } catch (e) {
    console.error(e);
  }

  /** It is most likely that symbol '-' is not present in the proposal name then we use /\d+$/ */
  const numbersRegEXp = new RegExp(/\d+$/);
  /** This will get all numbers at the end of the string
   * Example: input=mx3030 then output[0]=3030
   */

  try {
    const res = numbersRegEXp.exec(proposalName);
    if (res?.length)
      return {
        proposalCode: proposalName.substring(
          0,
          proposalName.length - res[0].length,
        ),
        proposalNumber: res[0],
      };
  } catch (e) {
    console.error(e);
  }

  return undefined;
}
