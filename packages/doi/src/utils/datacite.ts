import {
  ABSTRACT_DESCRIPTION_TYPE,
  CATEGORY_SUBJECT_SCHEME,
  COLLECTED_DATE_TYPE,
  INSTRUMENT_SUBJECT_SCHEME,
  ORCID_IDENTIFIER_SCHEME,
  PROPOSAL_SUBJECT_SCHEME,
  RELATED_IDENTIFIER_TYPE_IS_CITED_BY,
  RELATED_IDENTIFIER_TYPE_REFERENCES,
  RELATED_IDENTIFIER_TYPE_RELATED_DOI,
} from 'constants/doi';
import type {
  DataciteDOIAttributes,
  DataciteDOI,
  DataciteParticipant,
} from 'model/datacite';
import type { DOI, DOIParticipant } from 'model/doi';

export function convertDataciteToInternal(
  dataciteObject: DataciteDOIAttributes | DataciteDOI | undefined,
): DOI | undefined {
  if (!dataciteObject) return undefined;

  const doi =
    'data' in dataciteObject ? dataciteObject.data.attributes : dataciteObject;

  return {
    doi: doi.doi,
    title: doi.titles[0].title,
    creators: doi.creators?.map(makeParticipant),
    contributors: doi.contributors?.map(makeParticipant),
    publicationYear: parseIntOrUndefined(doi.publicationYear),
    instruments: doi.subjects
      .filter(
        (subject) =>
          subject.subjectScheme &&
          subject.subjectScheme.toLowerCase() ===
            INSTRUMENT_SUBJECT_SCHEME.toLowerCase(),
      )
      .flatMap((subject) => subject.subject.split(',')),
    proposals: doi.subjects
      .filter(
        (subject) =>
          subject.subjectScheme &&
          subject.subjectScheme.toLowerCase() ===
            PROPOSAL_SUBJECT_SCHEME.toLowerCase(),
      )
      .flatMap((subject) => subject.subject.split(',')),
    description: doi.descriptions.find(
      (description) =>
        description.descriptionType.toLowerCase() ===
        ABSTRACT_DESCRIPTION_TYPE.toLowerCase(),
    )?.description,
    publisher: doi.publisher,
    collectionDate: doi.dates.find(
      (date) =>
        date.dateType.toLowerCase() === COLLECTED_DATE_TYPE.toLowerCase(),
    )?.date,
    categories: doi.subjects
      .filter(
        (subject) =>
          subject.subjectScheme &&
          subject.subjectScheme.toLowerCase() ===
            CATEGORY_SUBJECT_SCHEME.toLowerCase(),
      )
      .map((subject) => subject.subject),
    relatedDOI: doi.relatedIdentifiers
      ?.filter(
        (relatedIdentifier) =>
          relatedIdentifier.relatedIdentifierType ===
          RELATED_IDENTIFIER_TYPE_RELATED_DOI,
      )
      .map((relatedIdentifier) => relatedIdentifier.relatedIdentifier),
    resourceType: doi.types.resourceType,
    createdDate: doi.created,
    keywords: doi.subjects
      .filter((subject) => !subject.subjectScheme)
      .map((subject) => subject.subject),
    citationPublicationDOI: doi.relatedIdentifiers
      ?.filter(
        (relatedIdentifier) =>
          relatedIdentifier.relationType ===
          RELATED_IDENTIFIER_TYPE_IS_CITED_BY,
      )
      .map((relatedIdentifier) => relatedIdentifier.relatedIdentifier)?.[0],
    referenceURL: doi.relatedIdentifiers
      ?.filter(
        (relatedIdentifier) =>
          relatedIdentifier.relationType === RELATED_IDENTIFIER_TYPE_REFERENCES,
      )
      .map((relatedIdentifier) => relatedIdentifier.relatedIdentifier)?.[0],
  };
}

function makeParticipant(p: DataciteParticipant): DOIParticipant {
  const orcid = p.nameIdentifiers?.find(
    (i) =>
      i.nameIdentifierScheme.toLowerCase() ===
      ORCID_IDENTIFIER_SCHEME.toLowerCase(),
  );
  const name =
    p.familyName && p.givenName ? `${p.givenName} ${p.familyName}` : p.name;
  return {
    name: name,
    orcid: orcid ? orcid.nameIdentifier : undefined,
    contributorType: p.contributorType,
  };
}

function parseIntOrUndefined(value: string | number | undefined) {
  if (value === undefined) return undefined;
  const res = typeof value === 'string' ? parseInt(value) : value;
  if (isNaN(res)) return undefined;
  return res;
}
