import {
  ES_RESOURCE_TYPE,
  OPEN_ACCESS,
  RESTRICTED_ACCESS,
  DC_RESOURCE_TYPE,
  COLLECTION_DATE_FORMAT,
} from 'constants/doi';
import { parse, setYear } from 'date-fns';
import type { DOI } from 'model/doi';

export function getAccessibilityStatus(doi: DOI) {
  const { resourceType, collectionDate, publicationYear } = doi;
  if (resourceType === ES_RESOURCE_TYPE) {
    if (publicationYear && collectionDate) {
      // take day and month of sessionDate and year of publicationYear
      const publiclyAvailableDate = setYear(
        parse(collectionDate, COLLECTION_DATE_FORMAT, new Date()),
        publicationYear,
      );

      const now = new Date();

      if (now > publiclyAvailableDate) {
        return OPEN_ACCESS;
      } else if (now <= publiclyAvailableDate) {
        return RESTRICTED_ACCESS;
      }
    }
  } else if (resourceType === DC_RESOURCE_TYPE) {
    return OPEN_ACCESS;
  }
  return undefined;
}
