import { DOIExperimentReportESRF } from 'components/DOIInfo/DOIExperimentReportESRF';
import type { Server } from 'fetch/fetching';
import type { DOI } from 'model/doi';

interface Config {
  SITE_PUBLISHER_NAME: string;
  LANGING_PAGE_LINK: string;
  DATA_ACCESS_LINK_INVESTIGATION: string;
  DATA_ACCESS_LINK_DOI: string;
  DOWNLOAD_REPORT_LINK: string;
  PUBLISHER_URL: string;
  USER_PORTAL_URL: string;
  // value for each key of type Server is a string
  SERVERS: Record<Server, string>;
  EXPERIMENT_REPORT:
    | {
        enabled: false;
      }
    | {
        enabled: true;
        component: React.FC<{ doi: DOI }>;
      };
}

export const CONFIG: Config = {
  SITE_PUBLISHER_NAME: 'European Synchrotron Radiation Facility',
  LANGING_PAGE_LINK: 'https://doi.org/',
  DATA_ACCESS_LINK_INVESTIGATION:
    'https://data.esrf.fr/investigation/:id/datasets',
  DATA_ACCESS_LINK_DOI: 'https://data.esrf.fr/doi/:id/datasets',
  DOWNLOAD_REPORT_LINK: 'http://ftp.esrf.fr/pub/UserReports/',
  PUBLISHER_URL: 'https://www.esrf.fr/',
  USER_PORTAL_URL: 'https://smis.esrf.fr',
  SERVERS: {
    DATACITE: 'https://api.datacite.org/',
    CITATION: 'https://citation.doi.org/',
    USER_PORTAL: 'https://smis.esrf.fr/misapps/SMISServer/rest/',
  },
  EXPERIMENT_REPORT: {
    enabled: true,
    component: DOIExperimentReportESRF,
  },
};
