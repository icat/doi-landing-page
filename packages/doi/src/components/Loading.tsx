import { Spinner } from 'react-bootstrap';

export function Loading() {
  return (
    <div className="text-center">
      <Spinner animation="border" role="status" variant="primary" />
      <p className="">Loading...</p>
    </div>
  );
}
