import { Suspense } from 'react';
import { useLocalGetEndpoint } from 'fetch/fetching';
import { DATACITE_GET_DOI } from 'fetch/datacite';
import { ICATPLUS_GET_DOI, useGetEndpoint } from '@edata-portal/icat-plus-api';
import { Loading } from 'components/Loading';
import { DOIInfo } from 'components/DOIInfo/DOIInfo';
import { convertDataciteToInternal } from 'utils/datacite';
import { DOINotFound } from 'components/DOINotFound';

export function FetchAndDisplayDOI({ doi }: { doi: string }) {
  return (
    <Suspense fallback={<Loading />}>
      <FetchDOI doi={doi} />
    </Suspense>
  );
}

function FetchDOI({ doi }: { doi: string }) {
  const icatplusDOI = useGetEndpoint({
    endpoint: ICATPLUS_GET_DOI,
    params: { doi },
    notifyOnError: false,
  });

  const dataciteDOI = useLocalGetEndpoint({
    endpoint: DATACITE_GET_DOI,
    params: { doi },
    skipFetch: icatplusDOI !== undefined,
  });

  const doiObject = convertDataciteToInternal(
    icatplusDOI ? icatplusDOI : dataciteDOI,
  );
  if (!doiObject) return <DOINotFound />;

  return <DOIInfo doi={doiObject} />;
}
