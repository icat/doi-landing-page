import { Alert } from 'react-bootstrap';

export function DOINotFound() {
  return <Alert variant="danger">DOI not found</Alert>;
}
