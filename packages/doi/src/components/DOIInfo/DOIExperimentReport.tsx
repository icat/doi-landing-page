import { CONFIG } from 'config/config';
import type { DOI } from 'model/doi';
import { Card } from 'react-bootstrap';

export function DOIExperimentReport({ doi }: { doi: DOI }) {
  if (!CONFIG.EXPERIMENT_REPORT.enabled) return null;

  const ReportComponent = CONFIG.EXPERIMENT_REPORT.component;

  return (
    <Card className="h-100">
      <Card.Header className="p-2">Experimental report</Card.Header>
      <ReportComponent doi={doi} />
    </Card>
  );
}
