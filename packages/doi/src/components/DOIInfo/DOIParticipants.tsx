import { UsersList } from 'components/DOIInfo/UsersList';
import type { DOI } from 'model/doi';
import { Row } from 'react-bootstrap';

export function DOIParticipants({ doi }: { doi: DOI }) {
  const doiContributors = doi.contributors?.length ? doi.contributors : [];
  const contributors = doiContributors.filter(
    (contributor) => contributor.contributorType === 'DataCollector',
  );
  const principalInvestigators = doiContributors.filter(
    (contributor) => contributor.contributorType === 'ProjectManager',
  );
  const coInvestigators = doiContributors.filter(
    (contributor) => contributor.contributorType === 'ProjectMember',
  );

  return (
    <Row className="g-2">
      <UsersList users={doi.creators} />
      <UsersList users={contributors} title="Contributors" />
      <UsersList
        users={principalInvestigators}
        title="Principal Investigator"
      />
      <UsersList users={coInvestigators} title="Co-Investigators:" />
    </Row>
  );
}
