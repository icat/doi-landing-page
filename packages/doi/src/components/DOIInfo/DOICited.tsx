import { DOICitationPublication } from 'components/DOIInfo/DOICItationPublication';
import type { DOI } from 'model/doi';
import { Card } from 'react-bootstrap';

export function DOICited({ doi }: { doi: DOI }) {
  if (!doi.citationPublicationDOI) {
    return null;
  }
  return (
    <Card>
      <Card.Header className="p-2">Cited by</Card.Header>
      <Card.Body className="p-0">
        <DOICitationPublication doi={doi} />
      </Card.Body>
    </Card>
  );
}
