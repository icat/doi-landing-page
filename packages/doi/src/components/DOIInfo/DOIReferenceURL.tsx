import { CopyValue } from 'components/CopyValue';
import type { DOI } from 'model/doi';

export function DOIReferenceURL({ doi }: { doi: DOI }) {
  if (!doi.referenceURL) {
    return null;
  }
  const renderedValue = (
    <a href={doi.referenceURL} target="_blank" rel="noopener noreferrer">
      {doi.referenceURL}
    </a>
  );
  return <CopyValue value={doi.referenceURL} renderedValue={renderedValue} />;
}
