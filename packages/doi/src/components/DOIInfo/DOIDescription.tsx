import type { DOI } from 'model/doi';

export function DOIDescription({ doi }: { doi: DOI }) {
  return (
    <pre
      className="m-0"
      style={{
        textAlign: 'justify',
        whiteSpace: 'pre-wrap',
        wordWrap: 'break-word',
        fontFamily: 'inherit',
        overflowWrap: 'break-word',
        overflowX: 'hidden',
      }}
    >
      {doi.description}
    </pre>
  );
}
