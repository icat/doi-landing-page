import { DOICited } from 'components/DOIInfo/DOICited';
import { DOIDataAccess } from 'components/DOIInfo/DOIDataAccess';
import { DOIDescription } from 'components/DOIInfo/DOIDescription';
import { DOIMetatada } from 'components/DOIInfo/DOIMetatada';
import { DOIParticipants } from 'components/DOIInfo/DOIParticipants';
import { DOIReference } from 'components/DOIInfo/DOIReference';
import { DOIRelated } from 'components/DOIInfo/DOIRelated';
import { DOIExperimentReport } from 'components/DOIInfo/DOIExperimentReport';
import { DOITitle } from 'components/DOIInfo/DOITitle';
import { DOITypes } from 'components/DOIInfo/DOITypes';
import type { DOI } from 'model/doi';
import { Col, Row } from 'react-bootstrap';
import { CONFIG } from 'config/config';
import { DOIKeywords } from 'components/DOIInfo/DOIKeywords';
import { DOIReferencesIn } from 'components/DOIInfo/DOIReferencesIn';

export function DOIInfo({ doi }: { doi: DOI }) {
  return (
    <>
      <Row>
        <DOITypes doi={doi} />
      </Row>
      <Row className="g-2">
        <Col xs={12} lg={7} xl={8} xxl={9}>
          <Row className="g-2">
            <Col xs={12}>
              <DOITitle doi={doi} />
            </Col>
            <Col xs={12}>
              <DOIParticipants doi={doi} />
            </Col>
            <Col xs={12}>
              <DOIDescription doi={doi} />
            </Col>
            <Col xs={12}>
              <DOIKeywords doi={doi} />
            </Col>
            <Col xs={12} sm={CONFIG.EXPERIMENT_REPORT.enabled ? 6 : 12}>
              <DOIDataAccess doi={doi} />
            </Col>
            {CONFIG.EXPERIMENT_REPORT.enabled ? (
              <Col xs={12} sm={6}>
                <DOIExperimentReport doi={doi} />
              </Col>
            ) : null}
            <Col xs={12}>
              <DOIReference doi={doi} />
            </Col>
            <Col xs={12}>
              <DOICited doi={doi} />
            </Col>
            <Col xs={12}>
              <DOIReferencesIn doi={doi} />
            </Col>
          </Row>
        </Col>
        <Col xs={12} lg={5} xl={4} xxl={3}>
          <Row className="g-2">
            <Col xs={12}>
              <DOIMetatada doi={doi} />
            </Col>
            <Col xs={12}>
              <DOIRelated doi={doi} />
            </Col>
          </Row>
        </Col>
      </Row>
    </>
  );
}
