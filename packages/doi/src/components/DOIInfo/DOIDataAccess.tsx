import { CONFIG } from 'config/config';
import {
  RESTRICTED_ACCESS,
  ES_RESOURCE_TYPE,
  OPEN_ACCESS,
} from 'constants/doi';
import type { DOI } from 'model/doi';

import { useMemo } from 'react';
import { Button, Card } from 'react-bootstrap';
import { getAccessibilityStatus } from 'utils/accessibility';

export function DOIDataAccess({ doi }: { doi: DOI }) {
  const investigationId = useMemo(() => {
    const regExp = RegExp(/\/\w*-*[Ee][Ss][Rr][Ff]-[Ee][Ss]-(\d+)$/);
    const res = regExp.exec(doi.doi);
    if (res) {
      return res[1];
    }
    return undefined;
  }, [doi]);

  const accessibilityStatus = useMemo(() => getAccessibilityStatus(doi), [doi]);

  const accessDataButtonText = useMemo(
    () =>
      accessibilityStatus === RESTRICTED_ACCESS
        ? 'Access data for experimental team'
        : 'Access data',
    [accessibilityStatus],
  );

  const dataUrl = useMemo(
    () =>
      doi.resourceType === ES_RESOURCE_TYPE
        ? CONFIG.DATA_ACCESS_LINK_INVESTIGATION.replace(
            ':id',
            investigationId || 'undefined',
          )
        : CONFIG.DATA_ACCESS_LINK_DOI.replace(':id', doi.doi),
    [doi, investigationId],
  );

  return (
    <Card className="h-100">
      <Card.Header className="p-2">Experimental data</Card.Header>
      <Card.Body className="p-2">
        {accessibilityStatus === OPEN_ACCESS && (
          <Card.Text>
            Data can be accessed by clicking on the link below
          </Card.Text>
        )}
        {accessibilityStatus === RESTRICTED_ACCESS && (
          <Card.Text>
            Data is under embargo until <b>{doi.publicationYear}</b> but could
            be released earlier. Currently, it is only accessible to proposal
            team members.
          </Card.Text>
        )}
        <Button
          variant="primary"
          href={dataUrl}
          target="_blank"
          rel="noopener noreferrer"
          size="sm"
        >
          {accessDataButtonText}
        </Button>
      </Card.Body>
    </Card>
  );
}
