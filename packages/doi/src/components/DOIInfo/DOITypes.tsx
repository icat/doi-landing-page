import {
  ES_RESOURCE_TYPE,
  DC_RESOURCE_TYPE,
  OPEN_ACCESS,
  RESTRICTED_ACCESS,
} from 'constants/doi';
import type { DOI } from 'model/doi';

import { Badge, Stack } from 'react-bootstrap';
import { getAccessibilityStatus } from 'utils/accessibility';

export function DOITypes({ doi }: { doi: DOI }) {
  const accessibilityStatus = getAccessibilityStatus(doi);

  return (
    <Stack
      direction="horizontal"
      style={{
        fontSize: '1.3rem',
      }}
      gap={2}
    >
      {doi.resourceType === ES_RESOURCE_TYPE && (
        <Badge bg="secondary">Session</Badge>
      )}
      {doi.resourceType === DC_RESOURCE_TYPE && (
        <Badge bg="secondary">Data Collection</Badge>
      )}
      {accessibilityStatus === OPEN_ACCESS && (
        <Badge bg="success">Open access</Badge>
      )}
      {accessibilityStatus === RESTRICTED_ACCESS && (
        <Badge bg="warning">Restricted access</Badge>
      )}
    </Stack>
  );
}
