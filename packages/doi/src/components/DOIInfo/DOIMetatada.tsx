import { DOIBadge } from 'components/DOIInfo/DOIBadge';
import { CONFIG } from 'config/config';
import {
  INSTRUMENT_LIST_ENDPOINT,
  useGetEndpoint,
} from '@edata-portal/icat-plus-api';
import type { DOI } from 'model/doi';
import { Suspense } from 'react';
import { Card, Col, Row } from 'react-bootstrap';
import { findInstrumentUrl } from 'utils/icatplus';
import { COLLECTION_DATE_FORMAT } from 'constants/doi';
import { format } from 'date-fns';

export function DOIMetatada({ doi }: { doi: DOI }) {
  const createdDate = doi.createdDate
    ? format(doi.createdDate, COLLECTION_DATE_FORMAT)
    : undefined;
  return (
    <Card>
      <Card.Header className="p-2">Metadata</Card.Header>
      <Card.Body className="pt-0 p-2">
        <Col>
          <MetadataElement
            title="Identifier"
            content={<DOIBadge doi={doi.doi} />}
          />

          {doi.proposals?.length && doi.instruments?.length && (
            <Row>
              {doi.proposals?.length && (
                <Col>
                  <MetadataElement
                    title={`Proposal${doi.proposals?.length > 1 ? 's' : ''}`}
                    content={doi.proposals}
                  />
                </Col>
              )}
              {doi.instruments?.length && (
                <Col>
                  <MetadataElement
                    title={`Beamline${doi.instruments?.length > 1 ? 's' : ''}`}
                    content={doi.instruments.map((i) => (
                      <Instrument key={i} instrument={i} />
                    ))}
                  />
                </Col>
              )}
            </Row>
          )}
          <MetadataElement
            title="Public release year"
            content={doi.publicationYear}
          />
          <MetadataElement title="Session date" content={doi.collectionDate} />
          <MetadataElement title="Category" content={doi.categories} />
          <MetadataElement
            title="Publisher"
            content={<Publisher doi={doi} />}
          />
          <MetadataElement title="License (for files)" content={<License />} />
          <MetadataElement title="Deposit date" content={createdDate} />
        </Col>
      </Card.Body>
    </Card>
  );
}

type MetadataType = string | number | JSX.Element | undefined | null;

function MetadataElement({
  title,
  content,
}: {
  title: string;
  content: MetadataType | MetadataType[];
}) {
  const contentArray = Array.isArray(content) ? content : [content];

  const filteredContentArray = contentArray.filter(
    (c) => c !== null && c !== undefined,
  );

  if (filteredContentArray.length === 0) return null;

  return (
    <>
      <Row className="mt-2">
        <strong>{title}</strong>
      </Row>

      {filteredContentArray.map((c, i) => (
        <Row key={i}>
          <Col>{c}</Col>
        </Row>
      ))}
    </>
  );
}

function License() {
  return (
    <a
      href="https://creativecommons.org/licenses/by/4.0/"
      target="_blank"
      rel="noopener noreferrer"
    >
      Creative Commons Attribution 4.0
    </a>
  );
}

function Publisher({ doi }: { doi: DOI }) {
  if (
    doi.publisher?.toLocaleLowerCase() !==
    CONFIG.SITE_PUBLISHER_NAME.toLocaleLowerCase()
  )
    return <>{doi.publisher}</>;
  return (
    <a href={CONFIG.PUBLISHER_URL} target="_blank" rel="noopener noreferrer">
      {doi.publisher}
    </a>
  );
}

function Instrument({ instrument }: { instrument: string }) {
  return (
    <Suspense fallback={instrument}>
      <LoadInstrumentUrl instrument={instrument} />
    </Suspense>
  );
}

function LoadInstrumentUrl({ instrument }: { instrument: string }) {
  const icatPlusInstruments = useGetEndpoint({
    endpoint: INSTRUMENT_LIST_ENDPOINT,
  });

  if (!icatPlusInstruments) return <>{instrument}</>;

  const url = findInstrumentUrl(icatPlusInstruments, instrument);

  if (!url) return <>{instrument}</>;

  return (
    <a href={url} target="_blank" rel="noopener noreferrer">
      {instrument}
    </a>
  );
}
