import type { DOIParticipant } from 'model/doi';
import { Col, Row } from 'react-bootstrap';
import orcid from 'assets/orcid.png';

export function UsersList({
  users,
  title,
}: {
  users?: DOIParticipant[];
  title?: string;
}) {
  return (
    <>
      {users?.length ? (
        <Col xs={12}>
          <Row className="gx-2">
            {title && (
              <Col xs={'auto'}>
                <small>{title}:</small>
              </Col>
            )}
            {users.map((participant, index, self) => (
              <Col xs={'auto'} key={participant.name}>
                <Participant participant={participant} />
                {index < self.length - 1 && (
                  <span className="text-muted">;</span>
                )}
              </Col>
            ))}
          </Row>
        </Col>
      ) : null}
    </>
  );
}

function Participant({ participant }: { participant: DOIParticipant }) {
  return (
    <span>
      <small className="text-muted text-nowrap">{participant.name}</small>
      <ORCIDBadge participant={participant} />
    </span>
  );
}

function ORCIDBadge({ participant }: { participant: DOIParticipant }) {
  if (!participant.orcid) {
    return null;
  }
  return (
    <a
      href={participant.orcid}
      target="_blank"
      rel="noopener noreferrer"
      className="ms-1"
    >
      <img src={orcid} alt="ORCID" />
    </a>
  );
}
