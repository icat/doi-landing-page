import { CITATION_FORMAT_REFERENCE } from 'fetch/citation';
import { useLocalGetEndpoint } from 'fetch/fetching';
import type { DOI } from 'model/doi';
import { Alert, Card } from 'react-bootstrap';
import Autolinker from 'autolinker';
import { Suspense } from 'react';
import { Loading } from 'components/Loading';
import { CopyValue } from 'components/CopyValue';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faWarning } from '@fortawesome/free-solid-svg-icons';

export function DOIReference({ doi }: { doi: DOI }) {
  return (
    <Card>
      <Card.Header className="p-2">Reference</Card.Header>
      <Card.Body className=" p-2">
        <p>
          Researchers must acknowledge the source of the data and cite its
          unique identifier as well as any publications linked to the same raw
          data.
          <br />
          Below is the recommended format for citing this work in a research
          publication.
        </p>

        <Suspense fallback={<Loading />}>
          <LoadReference doi={doi.doi} copy />
        </Suspense>
      </Card.Body>
    </Card>
  );
}

export function LoadReference({ doi, copy }: { doi: string; copy?: boolean }) {
  const reference = useLocalGetEndpoint({
    endpoint: CITATION_FORMAT_REFERENCE,
    params: {
      doi: doi,
      lang: 'en-US',
      style: 'apa',
    },
  });

  if (!reference)
    return (
      <Alert variant="warning" className="p-2">
        <FontAwesomeIcon icon={faWarning} className="me-2" />
        Could not get reference
      </Alert>
    );

  const content = (
    <p
      className="m-0"
      style={{
        fontStyle: 'italic',
        wordBreak: 'keep-all',
        textAlign: 'justify',
      }}
      dangerouslySetInnerHTML={
        reference
          ? {
              __html: Autolinker.link(reference),
            }
          : undefined
      }
    />
  );

  if (!copy) return content;
  return <CopyValue value={reference} renderedValue={content} />;
}
