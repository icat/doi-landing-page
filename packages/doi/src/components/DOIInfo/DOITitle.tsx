import type { DOI } from 'model/doi';

export function DOITitle({ doi }: { doi: DOI }) {
  return <h3>{doi.title}</h3>;
}
