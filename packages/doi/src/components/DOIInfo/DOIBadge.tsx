import { CONFIG } from 'config/config';

export function DOIBadge({
  doi,
  interactive = true,
  reserved = false,
}: {
  doi?: string;
  interactive?: boolean;
  reserved?: boolean;
}) {
  if (!doi?.trim().length) return null;
  if (reserved) {
    interactive = false;
  }

  const doiUrl = CONFIG.LANGING_PAGE_LINK + doi;

  const doiLabel = (
    <div
      className="bg-black text-white"
      style={{
        padding: '2px 5px',
        display: 'inline-flex',
        alignItems: 'center',
      }}
      key={'doilabel'}
    >
      DOI
    </div>
  );

  const bg = reserved ? 'bg-warning' : 'bg-info';
  const className = `${bg} text-white text-decoration-none`;
  const doiLink = (
    <a
      href={interactive ? doiUrl : undefined}
      target={interactive ? '_blank' : undefined}
      rel={interactive ? 'noopener noreferrer' : undefined}
      className={className}
      style={{
        padding: '2px 5px',
        display: 'inline-flex',
        alignItems: 'center',
      }}
      key={'doilink'}
    >
      {doi.toLocaleUpperCase()}
    </a>
  );

  return (
    <div
      className="rounded"
      style={{
        display: 'inline-flex',
        overflow: 'hidden',
      }}
    >
      {doiLabel}
      {doiLink}
    </div>
  );
}
