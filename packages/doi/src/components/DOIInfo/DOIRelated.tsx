import { LoadReference } from 'components/DOIInfo/DOIReference';
import { Loading } from 'components/Loading';
import type { DOI } from 'model/doi';
import { Suspense } from 'react';
import { Card } from 'react-bootstrap';

export function DOIRelated({ doi }: { doi: DOI }) {
  return (
    <Card>
      <Card.Header className="p-2">Related DOIs</Card.Header>
      <Card.Body className="p-0">
        {doi.relatedDOI?.length ? (
          doi.relatedDOI?.map((doi, index, self) => (
            <div
              key={doi}
              className={`p-2 ${
                index === self.length - 1 ? '' : 'border-bottom border-black'
              }`}
            >
              <Suspense fallback={<Loading />}>
                <LoadReference doi={doi} />
              </Suspense>
            </div>
          ))
        ) : (
          <p className="p-2 m-0">No related DOIs were found.</p>
        )}
        {}
      </Card.Body>
    </Card>
  );
}
