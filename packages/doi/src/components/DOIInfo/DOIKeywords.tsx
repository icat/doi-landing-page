import type { DOI } from 'model/doi';
import { Badge } from 'react-bootstrap';

export function DOIKeywords({ doi }: { doi: DOI }) {
  if (!doi.keywords) {
    return null;
  }
  return (
    <>
      {doi.keywords.map((keyword, index) => (
        <Badge
          key={`id=keyword-${index}`}
          bg="secondary"
          style={{ marginRight: '8px' }}
        >
          {keyword}
        </Badge>
      ))}
    </>
  );
}
