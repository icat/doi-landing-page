import { CopyValue } from 'components/CopyValue';
import type { DOI } from 'model/doi';

export function DOICitationPublication({ doi }: { doi: DOI }) {
  if (!doi.citationPublicationDOI) {
    return null;
  }
  const publicationDOI = `https://doi.org/${doi.citationPublicationDOI}`;
  const renderedValue = (
    <a href={publicationDOI} target="_blank" rel="noopener noreferrer">
      {publicationDOI}
    </a>
  );
  return <CopyValue value={publicationDOI} renderedValue={renderedValue} />;
}
