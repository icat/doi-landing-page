import { Loading } from 'components/Loading';
import { CONFIG } from 'config/config';
import {
  useGetEndpoint,
  ICATPLUS_GET_EXP_REPORTS,
} from '@edata-portal/icat-plus-api';
import type { ExpReport } from 'model/icatplus';
import type { DOI } from 'model/doi';
import { Suspense } from 'react';
import { Button, Card, Col } from 'react-bootstrap';

export function DOIExperimentReportESRF({ doi }: { doi: DOI }) {
  return (
    <Suspense fallback={<Loading />}>
      <LoadReports doi={doi} />
    </Suspense>
  );
}

function LoadReports({ doi }: { doi: DOI }) {
  const reportFiles = useGetEndpoint({
    endpoint: ICATPLUS_GET_EXP_REPORTS,
    params: { doi: doi.doi.toUpperCase() },
  });

  if (!reportFiles || reportFiles.length === 0) {
    return null;
  }

  return (
    <>
      {reportFiles.map((dataReport, index) => (
        <Card.Body key={`card-report-${index}`} className="p-2">
          <ReportText dataReport={dataReport}></ReportText>

          {!dataReport.underEmbargo && (
            <>
              {dataReport.reports.map((report, idR) => (
                <Col xs={'auto'} key={report}>
                  <ReportButton file={report} />
                </Col>
              ))}
            </>
          )}
        </Card.Body>
      ))}
    </>
  );
}

function ReportButton({ file }: { file: string }) {
  const link = `${CONFIG.DOWNLOAD_REPORT_LINK}${file}`;
  return (
    <Button href={link} size="sm" target="_blank" rel="noopener noreferrer">
      {file}
    </Button>
  );
}

function ReportText({ dataReport }: { dataReport: ExpReport }) {
  const underEmbargo = dataReport.underEmbargo;
  const hasMultipleReports = dataReport.nbReports > 1;
  const hasNoReport = dataReport.nbReports === 0;
  const esrfUserPortal = `<a href="${CONFIG.USER_PORTAL_URL}" target="_blank" rel="noopener noreferrer">ESRF User Portal.</a>`;
  let text;
  if (hasNoReport) {
    text = `No report was found for ${dataReport.proposal}. Proposers and session participants may submit it via the ${esrfUserPortal}`;
  } else {
    if (underEmbargo) {
      text = hasMultipleReports
        ? `The reports for ${dataReport.proposal} are confidential until the end of the data embargo period. Until then, they are only accessible to the proposers and session participants via the ${esrfUserPortal}`
        : `The report for ${dataReport.proposal} is confidential until the end of the data embargo period. Until then, it is only accessible to the proposers and session participants via the ${esrfUserPortal}`;
    } else {
      text = hasMultipleReports
        ? `In total,<b> ${dataReport.nbReports}</b> reports have been found for ${dataReport.proposal}.`
        : `<b>One</b> report has been found for ${dataReport.proposal}.`;
    }
  }
  return (
    <div
      dangerouslySetInnerHTML={{
        __html: text,
      }}
    ></div>
  );
}
