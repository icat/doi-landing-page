import { DOIReferenceURL } from 'components/DOIInfo/DOIReferenceURL';
import type { DOI } from 'model/doi';
import { Card } from 'react-bootstrap';

export function DOIReferencesIn({ doi }: { doi: DOI }) {
  if (!doi.referenceURL) {
    return null;
  }
  return (
    <Card>
      <Card.Header className="p-2">References</Card.Header>
      <Card.Body className="p-0">
        <DOIReferenceURL doi={doi} />
      </Card.Body>
    </Card>
  );
}
