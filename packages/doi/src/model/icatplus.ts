export interface Instrument {
  id: number;
  createId: string;
  createTime: string;
  modId: string;
  modTime: string;
  description: string;
  fullName: string;
  name: string;
  type: string;
  url: string;
}

export interface ExpReport {
  proposal: string;
  nbReports: number;
  underEmbargo: boolean;
  reports: string[];
}
