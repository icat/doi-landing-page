export interface NameIdentifier {
  schemeUri: string;
  nameIdentifierScheme: string;
  nameIdentifier: string;
}

export interface DataciteParticipant {
  name: string;
  givenName?: string;
  familyName?: string;
  nameIdentifiers?: NameIdentifier[];
  contributorType?: string;
}

export interface DataciteTitle {
  title: string;
  titleType?: string;
}

export interface DataciteDescription {
  description: string;
  descriptionType: string;
}

export interface DataciteSubject {
  subject: string;
  subjectScheme: string;
}

export interface DataciteDateObject {
  date: string;
  dateType: string;
}

export interface DataciteTypes {
  resourceTypeGeneral: string;
  resourceType: string;
}

export interface DataciteRelatedIdentifier {
  relatedIdentifier: string;
  relatedIdentifierType: string;
  relationType: string;
}

export interface DataciteDOIAttributes {
  doi: string;
  url: string;
  publicationYear: string | number;
  creators?: DataciteParticipant[];
  contributors?: DataciteParticipant[];
  titles: DataciteTitle[];
  descriptions: DataciteDescription[];
  publisher: string;
  subjects: DataciteSubject[];
  dates: DataciteDateObject[];
  relatedIdentifiers?: DataciteRelatedIdentifier[];
  types: DataciteTypes;
  created?: string;
}
export interface DataciteDOI {
  data: {
    id: string;
    type: string;
    attributes: DataciteDOIAttributes;
  };
}
