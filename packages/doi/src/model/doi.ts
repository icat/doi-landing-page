export type DOI = {
  doi: string;
  title: string;
  creators?: DOIParticipant[];
  contributors?: DOIParticipant[];
  publicationYear?: number;
  instruments: string[];
  proposals: string[];
  description?: string;
  publisher?: string;
  collectionDate?: string;
  categories?: string[];
  relatedDOI?: string[];
  resourceType?: string;
  createdDate?: string;
  keywords?: string[];
  citationPublicationDOI?: string;
  referenceURL?: string;
};
export type DOIParticipant = {
  name: string;
  orcid?: string;
  contributorType?: string;
};
