import { DOI_PREFIX_PARAM, DOI_SUFFIX_PARAM } from 'hooks/path';
import { LandingPage } from 'landingPage/LandingPage';
import { WelcomeLandingPage } from 'landingPage/WelcomeLandingPage';
import type { RouteObject } from 'react-router-dom';
import { PageNotFound } from 'routing/PageNotFound';

const DOI_ROUTE = {
  path: `:${DOI_PREFIX_PARAM}/:${DOI_SUFFIX_PARAM}`,
  element: <LandingPage />,
};

const WELCOME_ROUTE = {
  path: '',
  element: <WelcomeLandingPage />,
};

export const routes: RouteObject[] = [
  {
    path: '/',
    element: <WelcomeLandingPage />,
  },
  {
    path: 'doi',
    children: [WELCOME_ROUTE, DOI_ROUTE],
  },
  DOI_ROUTE,
  {
    path: '*',
    element: <PageNotFound />,
  },
];
