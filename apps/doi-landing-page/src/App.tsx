import { AppRouter } from 'routing/AppRouter';
import 'App.scss';
import { LandingPageContainer } from 'landingPage/LandingPageContainer';
import { useEffect, useMemo } from 'react';
import { CONFIG } from 'config/config';
import { IcatPlusAPIContext } from '@edata-portal/icat-plus-api';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';

const queryClient = new QueryClient();

function App() {
  useEffect(() => {
    document.title = `${CONFIG.SITE_NAME} DOI`;
  }, []);

  const value = useMemo(
    () =>
      ({
        baseUrl: CONFIG.ICATPLUS_URL || '',
        onError: console.error,
      }) as const,
    [],
  );

  return (
    <QueryClientProvider client={queryClient}>
      <IcatPlusAPIContext.Provider value={value}>
        <AppRouter>
          <LandingPageContainer />
        </AppRouter>
      </IcatPlusAPIContext.Provider>
    </QueryClientProvider>
  );
}

export default App;
