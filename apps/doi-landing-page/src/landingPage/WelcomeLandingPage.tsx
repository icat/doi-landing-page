import {
  Col,
  Container,
  Row,
  Card,
  Stack,
  Form,
  Button,
  Toast,
} from 'react-bootstrap';
import { CONFIG } from 'config/config';
import { useState } from 'react';

export function WelcomeLandingPage() {
  const landingPage = CONFIG.landingPage;
  const [searchedDOI, setSearchedDOI] = useState('');
  const [error, setError] = useState('');

  function searchDOI(doiPrefix: string, errorMessage: string) {
    if (!searchedDOI || !searchedDOI.startsWith(doiPrefix)) {
      setError(errorMessage);
      return;
    }
    window.open(`/doi/${searchedDOI}`, '_blank', 'noopener,noreferrer');
    setSearchedDOI('');
  }

  if (landingPage.length === 0) {
    return <></>;
  }
  return (
    <Container fluid>
      <Row style={{ textAlign: 'center' }}>
        <Col>
          <h1 className="text-primary">{CONFIG.SITE_NAME} DOI Portal</h1>
        </Col>
      </Row>
      <Row>
        {landingPage.map(({ title, texts, footer, search }, idC) => (
          <Col xs={12} md={4} key={`col-${idC}`}>
            <Card key={`card-${idC}`}>
              <Card.Header style={{ textAlign: 'center' }}>
                <h3 className="text-primary">{title}</h3>
              </Card.Header>
              <Card.Body>
                {texts.map(({ text }, idT) => (
                  <Card.Text key={`text-${idT}`}>
                    <span
                      dangerouslySetInnerHTML={{
                        __html: text,
                      }}
                    />
                  </Card.Text>
                ))}
                {footer && (
                  <Card.Footer>
                    <div
                      dangerouslySetInnerHTML={{
                        __html: footer,
                      }}
                    />
                  </Card.Footer>
                )}
                {search && (
                  <Card.Footer>
                    <Stack gap={2}>
                      <Stack direction="horizontal">
                        <Form.Control
                          type="text"
                          value={searchedDOI}
                          onChange={(e) => setSearchedDOI(e.target.value)}
                          placeholder={search.placeHolder}
                        />

                        <Button
                          variant="primary"
                          onClick={() =>
                            searchDOI(CONFIG.doi.prefix, search.error)
                          }
                        >
                          Search
                        </Button>
                      </Stack>

                      {error && (
                        <Toast
                          onClose={() => setError('')}
                          delay={3000}
                          autohide
                          bg="danger"
                        >
                          <Toast.Header>
                            <strong className="me-auto">Search DOI</strong>
                          </Toast.Header>
                          <Toast.Body>{error}</Toast.Body>
                        </Toast>
                      )}
                    </Stack>
                  </Card.Footer>
                )}
              </Card.Body>
            </Card>
          </Col>
        ))}
      </Row>
    </Container>
  );
}
