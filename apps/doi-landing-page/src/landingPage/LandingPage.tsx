import { DOI } from '@edata-portal/doi';
import { useCurrentPathDOI } from 'hooks/path';
import { PageNotFound } from 'routing/PageNotFound';

export function LandingPage() {
  const doi = useCurrentPathDOI();

  if (!doi) {
    return <PageNotFound />;
  }

  return <DOI doi={doi} />;
}
