import { Col, Container, Navbar } from 'react-bootstrap';
import { Outlet } from 'react-router-dom';

export function LandingPageContainer() {
  return (
    <Col>
      <Navbar className="bg-primary">
        <Container>
          <Navbar.Brand
            style={{
              height: 50,
            }}
          />
        </Container>
      </Navbar>
      <Container className="pt-3 pb-3">
        <Outlet />
      </Container>
    </Col>
  );
}
