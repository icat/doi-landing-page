import { useParams } from 'react-router-dom';

export function usePath(key: string): string | undefined {
  const params = useParams();
  return params[key] || undefined;
}

export function useCurrentPathDOI() {
  const prefix = usePath(DOI_PREFIX_PARAM);
  const suffix = usePath(DOI_SUFFIX_PARAM);
  if (!prefix || !suffix) {
    return undefined;
  }
  return getDOI(prefix, suffix);
}

export const DOI_PREFIX_PARAM = 'doiPrefix';
export const DOI_SUFFIX_PARAM = 'doiSuffix';

function getDOI(prefix: string, suffix: string): string {
  return `${prefix}/${suffix}`;
}
