export const CONFIG = {
  SITE_NAME: 'ESRF',
  ICATPLUS_URL: 'https://icatplus.esrf.fr/',
  doi: {
    prefix: '10.15151',
  },
  landingPage: [
    {
      title: 'About the portal',
      texts: [
        {
          text: 'This page centralizes the<a href="https://www.esrf.fr/DOI" target="_blank" rel="noopener noreferrer"> DOI</a> services offered by the ESRF to access data stored at the facility as part of its data policy. Data referred to by ESRF DOIs are accessed via the ESRF data catalogue <a href="https://data.esrf.fr" target="_blank" rel="noopener noreferrer">data.esrf.fr.</a>',
        },
        {
          text: 'You can mint a DOI for a subset of data: <a href="https://youtu.be/dPeN855-Mu4" target="_blank" rel="noopener noreferrer">How to mint a DOI</a>',
        },
        {
          text: 'Please contact us:  <a href="mailTo: dataportalrequests@esrf.fr"> dataportalrequests@esrf.fr',
        },
      ],
      footer:
        'Access to data is governed by the <a href="https://www.esrf.fr/files/live/sites/www/files/about/organisation/ESRF%20data%20policy-web.pdf" target="_blank" rel="noopener noreferrer" >ESRF data policy</a>',
    },
    {
      title: 'DOI Resolver',
      texts: [
        {
          text: ' The search engine below only searches DOIs which have been minted at the ESRF.',
        },
        {
          text: ' Use <a href="https://doi.org" target="_blank" rel="noopener noreferrer"> doi.org</a> search engine for other DOIs.',
        },
        {
          text: '<a href="https://search.datacite.org/works?query=10.15151%2F*" target="_blank" rel="noopener noreferrer">List all ESRF DOIs</a>',
        },
      ],
      search: {
        placeHolder: '10.15151/ESRF-ES-90632078',
        error: 'Invalid ESRF DOI',
      },
    },
    {
      title: 'About DOI',
      texts: [
        {
          text: ' The Digital Object Identifier ( <a href="http://www.esrf.eu/home/UsersAndScience/UserGuide/esrf-data-policy/doi.html" target="_blank" rel="noopener noreferrer">DOI </a>) is a code composed of digits and characters. It identifies data in a unique and permanent way thereby allowing data to be referenced worldwide. The DOI has two parts, namely the DOI prefix and the DOI suffix separated by a slash ("/"). The ESRF prefix is 10.15151 followed by a slash and the ESRF suffix. Currently the following suffix(es) are defined:<ul> <li><pre>ESRF-ES - for a session at the ESRF</pre> </li><li><pre>ESRF-DC - for a subset of data at the ESRF</pre></li></ul>',
        },
        {
          text: 'Refer to the DOI <a href="http://www.esrf.eu/home/UsersAndScience/UserGuide/esrf-data-policy/doi/doi---concepts.html" target="_blank" rel="noopener noreferrer">concepts page</a> to find out what a session is.',
        },
      ],
    },
  ],
};
