import { Alert } from 'react-bootstrap';

export function PageNotFound() {
  return <Alert variant="danger">Page not found</Alert>;
}
