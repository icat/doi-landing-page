import { useMemo } from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';

import { routes } from 'routes';

export function AppRouter({ children }: { children?: JSX.Element }) {
  const router = useMemo(() => {
    return createBrowserRouter([
      {
        element: children,
        children: routes,
      },
    ]);
  }, [children]);

  return <RouterProvider router={router} />;
}
